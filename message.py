from libDiameter import *

class Message:
    def __init__(self, appid, cmd, flags=[], avps=[]):
        self.appid = appid
        if isinstance(cmd, str):
            cmd = dictCOMMANDname2code(cmd)
        self.cmd = cmd
        self.avps = avps
        self.flags = flags
    
    def encode_list(self, avps):
        enc_avps = []
        for i, j in avps:
            if isinstance(j, list):
                j = self.encode_list(j)
            enc_avps.append(encodeAVP(i, j))
        return enc_avps
    
    def generate(self):
        enc_avps = self.encode_list(self.avps)
        hdr = HDRItem()
        for i in self.flags:
            setFlags(hdr, i)
        hdr.cmd = self.cmd
        hdr.appId = self.appid
        initializeHops(hdr)
        msg = createReq(hdr, enc_avps)
        return msg

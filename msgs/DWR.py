from libDiameter import *
from message import Message

def create_pkg(config):
    return Message(0, "Device-Watchdog",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Origin-State-Id", config["origin-state-id"]),
        ])

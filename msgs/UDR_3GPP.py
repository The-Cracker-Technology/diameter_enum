from libDiameter import *
from message import Message
import tools

#TS 29.329 - 6.1.1

#~ < User-Data -Request> ::=	< Diameter Header: 306, REQ, PXY, 16777217 >
                            #~ < Session-Id >
                            #~ [ DRMP ]
                            #~ { Vendor-Specific-Application-Id }
                            #~ { Auth-Session-State }
                            #~ { Origin-Host }
                            #~ { Origin-Realm }
                            #~ [ Destination-Host ]
                            #~ { Destination-Realm }
                            #~ *[ Supported-Features ]
                            #~ { User-Identity }
                            #~ [ Wildcarded-Public-Identity ]
                            #~ [ Wildcarded-IMPU ]
                            #~ [ Server-Name ]
                            #~ *[ Service-Indication ]
                            #~ *{ Data-Reference }
                            #~ *[ Identity-Set ]
                            #~ [ Requested-Domain ]
                            #~ [ Current-Location ]
                            #~ *[ DSAI-Tag ] 
                            #~ [ Session-Priority ] 
                            #~ [ User-Name ]
                            #~ [ Requested-Nodes ]
                            #~ [ Serving-Node-Indication ]
                            #~ [ Pre-paging-Supported ] 
                            #~ [ Local-Time-Zone-Indication ] 
                            #~ [ UDR-Flags ]
                            #~ [ Call-Reference-Info ] 
                            #~ [ OC-Supported-Features ]
                            #~ *[ AVP ]
                            #~ *[ Proxy-Info ]
                            #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    ims = "ims.mnc%s.mcc%s.3gppnetwork.org" % (config['mnc'], config['mcc'])
    username = "%s@%s" % (config['msisdn'], ims)
    pubid = "sip:+%s" % (username)
    
    return Message(16777217, "User-Data",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777217)
            ] ),
            ("Origin-Host", config["SH"]["origin-host-mme"]),
            ("Origin-Realm", config["SH"]["origin-realm"]),
            ("Destination-Host", config["SH"]["destination-host-hss"]),
            ("Destination-Realm", config["SH"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Identity", [
                ("Public-Identity", pubid),
            ] ),
            ("Data-Reference", "USER_STATE"),
            ("Data-Reference", "IMSI"),
            ("Data-Reference", "LOCATION_INFORMATION"),
            ("Data-Reference", "SERVICE_LEVEL_TRACE_INFO"),
        ])

from libDiameter import *
from message import Message

#TS 29.329 - 6.1.5

#~ < Subscribe-Notifications-Request > ::=	< Diameter Header: 308, REQ, PXY, 16777217 >
                                    #~ < Session-Id >
                                    #~ [ DRMP ]
                                    #~ { Vendor-Specific-Application-Id }
                                    #~ { Auth-Session-State }
                                    #~ { Origin-Host }
                                    #~ { Origin-Realm }
                                    #~ [ Destination-Host ]
                                    #~ { Destination-Realm }
                                    #~ *[ Supported-Features ]
                                    #~ { User-Identity }
                                    #~ [ Wildcarded-Public-Identity ]
                                    #~ [ Wildcarded-IMPU ]
                                    #~ *[ Service-Indication ]
                                    #~ [ Send-Data-Indication ]
                                    #~ [ Server-Name ]
                                    #~ { Subs-Req-Type }
                                    #~ *{ Data-Reference }
                                    #~ *[ Identity-Set ]
                                    #~ [ Expiry-Time ]
                                    #~ *[ DSAI-Tag ] 
                                    #~ [One-Time-Notification]
                                    #~ [ User-Name ]
                                    #~ [ OC-Supported-Features ]
                                    #~ *[ AVP ]
                                    #~ *[ Proxy-Info ]
                                    #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777217, "Subscribe-Notifications",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777217)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
    #{ User-Identity }
    #{ Data-Reference }
        ])

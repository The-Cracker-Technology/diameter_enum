from libDiameter import *
from message import Message

#RFC4740 - 8.11

       #~ <PPR> ::= < Diameter Header: 288, REQ, PXY >
                 #~ < Session-Id >
                 #~ { Auth-Application-Id }
                 #~ { Auth-Session-State }
                 #~ { Origin-Host }
                 #~ { Origin-Realm }
                 #~ { Destination-Realm }
                 #~ { User-Name }
               #~ * [ SIP-User-Data ]
                 #~ [ SIP-Accounting-Information ]
                 #~ [ Destination-Host ]
                 #~ [ Authorization-Lifetime ]
                 #~ [ Auth-Grace-Period ]
               #~ * [ Proxy-Info ]
               #~ * [ Route-Record ]
               #~ * [ AVP ]

def create_pkg(config):
    return Message(6, "SIP-Push-Profile",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
            ("Auth-Session-State", 1),
    #{ User-Name }
        ])

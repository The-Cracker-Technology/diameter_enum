from libDiameter import *
from message import Message

def create_pkg(config):
    return Message(0, "Disconnect-Peer",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Disconnect-Cause", int(config["disconnect-cause"])),
        ])

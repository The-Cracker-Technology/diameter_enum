from libDiameter import *
from message import Message

#TS 29.173 - 6.2.3

#~ < LCS-Routing-Info-Request> ::=	< Diameter Header: 8388622, REQ, PXY, 16777291 >
                                #~ < Session-Id >
                                #~ [ Vendor-Specific-Application-Id ]
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ [ Destination-Host ]
                                #~ { Destination-Realm }
                                #~ [ User-Name ]
                                #~ [ MSISDN ]
                                #~ [ GMLC-Number ]
                                #~ *[ Supported-Features ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]
                                #~ *[ AVP ]
                                                    
def create_pkg(config):
    return Message(16777291, "3GPP-Routing-Info",
         [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777291)
            ] ),
            ("Origin-Host", config["SLH"]["origin-host"]),
            ("Origin-Realm", config["SLH"]["origin-realm"]),
            ("Destination-Host", config["SLH"]["destination-host-hss"]),
            ("Destination-Realm", config["SLH"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config["imsi"]),
        ])

from libDiameter import *
from message import Message

#RFC4006 - 3.1

#~ <Credit-Control-Request> ::= < Diameter Header: 272, REQ, PXY >
                                   #~ < Session-Id >
                                   #~ { Origin-Host }
                                   #~ { Origin-Realm }
                                   #~ { Destination-Realm }
                                   #~ { Auth-Application-Id }
                                   #~ { Service-Context-Id }
                                   #~ { CC-Request-Type }
                                   #~ { CC-Request-Number }
                                   #~ [ Destination-Host ]
                                   #~ [ User-Name ]
                                   #~ [ CC-Sub-Session-Id ]
                                   #~ [ Acct-Multi-Session-Id ]
                                   #~ [ Origin-State-Id ]
                                   #~ [ Event-Timestamp ]
                                  #~ *[ Subscription-Id ]
                                   #~ [ Service-Identifier ]
                                   #~ [ Termination-Cause ]
                                   #~ [ Requested-Service-Unit ]
                                   #~ [ Requested-Action ]
                                  #~ *[ Used-Service-Unit ]
                                   #~ [ Multiple-Services-Indicator ]
                                  #~ *[ Multiple-Services-Credit-Control ]
                                  #~ *[ Service-Parameter-Info ]
                                   #~ [ CC-Correlation-Id ]
                                   #~ [ User-Equipment-Info ]
                                  #~ *[ Proxy-Info ]
                                  #~ *[ Route-Record ]
                                  #~ *[ AVP ]

def create_pkg(config):
    return Message(4, "Credit-Control",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", dictCOMMANDname2code("Re-Auth")),
        ])

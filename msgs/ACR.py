from libDiameter import *
from message import Message

def create_pkg(config):
    return Message(3, "Accounting",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Accounting-Record-Type", int(config["accounting-record-type"])),
            ("Accounting-Record-Number", int(config["accounting-record-number"])),
        ])

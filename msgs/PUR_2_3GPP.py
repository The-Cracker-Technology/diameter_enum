from libDiameter import *
from message import Message

#TS 29.272 - 7.2.13

#~ < Purge-UE-Request> ::=	< Diameter Header: 321, REQ, PXY, 16777251 >
                        #~ < Session-Id >
                        #~ [ DRMP ]
                        #~ [ Vendor-Specific-Application-Id ]
                        #~ { Auth-Session-State }
                        #~ { Origin-Host }
                        #~ { Origin-Realm }
                        #~ [ Destination-Host ]
                        #~ { Destination-Realm }
                        #~ { User-Name }
                        #~ [ OC-Supported-Features ]
                        #~ [ PUR-Flags ]
                        #~ *[ Supported-Features ]
                        #~ [ EPS-Location-Information ]
                        #~ *[ AVP ]
                        #~ *[ Proxy-Info ]
                        #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777251, "3GPP-Purge-UE", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            #~ ("Destination-Host", config["S6A"]["destination-host"]),
            ("Destination-Realm", config["S6A"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config['imsi']),
            ("PUR-Flags", 1),
        ])

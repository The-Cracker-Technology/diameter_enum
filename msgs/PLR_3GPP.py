from libDiameter import *
from message import Message

#TS 29.172 - 7.3.1

#~ < Provide-Location-Request> ::=	< Diameter Header: 8388620, REQ, PXY, 16777255 >
                                #~ < Session-Id >
                                #~ [ Vendor-Specific-Application-Id ]
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ {Destination-Host }
								#~ { Destination-Realm }
                                #~ { SLg-Location-Type }
                                #~ [ User-Name ]
                                #~ [ MSISDN] 
                                #~ [ IMEI ] 
                                #~ { LCS-EPS-Client-Name }
                                #~ { LCS-Client-Type }
                                #~ [ LCS-Requestor-Name ]
                                 #~ [ LCS-Priority ]
                                #~ [ LCS-QoS ] 
                                #~ [ Velocity-Requested ]
                                #~ [ Supported-GAD-Shapes ]
                                #~ [ LCS-Service-Type-ID ]
                                #~ [ LCS-Codeword ]
                                #~ [ LCS-Privacy-Check-Non-Session ]
                                #~ [ LCS-Privacy-Check-Session ]
                                #~ [Service-Selection ]
                                #~ *[ Supported-Features ]
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777225, "3GPP-Provide-Location", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777225)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config["username"]),
            ("3GPP-Location-Type", "CURRENT_OR_LAST_KNOWN_LOCATION"),
        ])

from libDiameter import *
from message import Message

#TS 29.336 - 6.2.3

#~ < Subscriber-Information-Request> ::=	< Diameter Header: 8388641, REQ, PXY, 16777310 >
                                        #~ < Session-Id >
                                        #~ [ DRMP ]
                                        #~ { Auth-Session-State }
                                        #~ { Origin-Host }
                                        #~ { Origin-Realm }
                                        #~ [ Destination-Host ]
                                        #~ { Destination-Realm }
                                        #~ { User-Identifier }
                                        #~ [ Service-ID ]
                                        #~ [ SCS-Identity ]
                                        #~ [ Service-Parameters ]
                                        #~ { SIR-Flags }
                                        #~ [ OC-Supported-Features ]
                                        #~ *[ Supported-Features ]
                                        #~ *[ Proxy-Info ]
                                        #~ *[ Route-Record ]
                                        #~ *[ AVP ]

def create_pkg(config):
    return Message(16777310, "3GPP-Subscriber-Information", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["S6A"]["origin-host-mme"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["S6A"]["destination-host-hss"]),
            ("Destination-Realm", config["S6A"]["destination-realm-ims"]),
            ("Auth-Session-State", 1),
            ("User-Identifier", [
                ("User-Name", config["imsi"]),
            ] ),
            #~ ("S6-Service-ID", "DEVICE_TRIGGER"),
            #~ ("Service-Parameters", [
                #~ ("Application-Port-Identifier", 0x3e803e80),
            #~ ] ),
            ("SIR-Flags", 0x1),
        ])

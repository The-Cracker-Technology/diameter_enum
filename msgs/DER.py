from libDiameter import *
from message import Message

# RFC4072 - 3.1

#~ <Diameter-EAP-Request> ::= < Diameter Header: 268, REQ, PXY >
                                 #~ < Session-Id >
                                 #~ { Auth-Application-Id }
                                 #~ { Origin-Host }
                                 #~ { Origin-Realm }
                                 #~ { Destination-Realm }
                                 #~ { Auth-Request-Type }
                                 #~ [ Destination-Host ]
                                 #~ [ NAS-Identifier ]
                                 #~ [ NAS-IP-Address ]
                                 #~ [ NAS-IPv6-Address ]
                                 #~ [ NAS-Port ]
                                 #~ [ NAS-Port-Id ]
                                 #~ [ NAS-Port-Type ]
                                 #~ [ Origin-State-Id ]
                                 #~ [ Port-Limit ]
                                 #~ [ User-Name ]
                                 #~ { EAP-Payload }
                                 #~ [ EAP-Key-Name ]
                                 #~ [ Service-Type ]
                                 #~ [ State ]
                                 #~ [ Authorization-Lifetime ]
                                 #~ [ Auth-Grace-Period ]
                                 #~ [ Auth-Session-State ]
                                 #~ [ Callback-Number ]
                                 #~ [ Called-Station-Id ]
                                 #~ [ Calling-Station-Id ]
                                 #~ [ Originating-Line-Info ]
                                 #~ [ Connect-Info ]
                               #~ * [ Framed-Compression ]
                                 #~ [ Framed-Interface-Id ]
                                 #~ [ Framed-IP-Address ]
                               #~ * [ Framed-IPv6-Prefix ]
                                 #~ [ Framed-IP-Netmask ]
                                 #~ [ Framed-MTU ]
                                 #~ [ Framed-Protocol ]
                               #~ * [ Tunneling ]
                               #~ * [ Proxy-Info ]
                               #~ * [ Route-Record ]
                               #~ * [ AVP ]

def create_pkg(config):
    return Message(5, "Diameter-EAP",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 1),  # 0, 1, 3
            ("Auth-Request-Type", int(config["auth-request-type"])),
            ("EAP-Payload", ""),
        ])

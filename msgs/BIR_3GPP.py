from libDiameter import *
from message import Message

#TS 29.109 - 5.2

#~ < Bootstrapping-Info-Request> ::=   <Diameter Header: 310, REQ, PXY, 16777220 >
                                #~ < Session-Id >
                                #~ { Vendor-Specific-Application-Id }
                                #~ { Origin-Host } ; Address of NAF
                                #~ { Origin-Realm } ; Realm of NAF
                                #~ { Destination-Realm } ; Realm of BSF
                                #~ [ Destination-Host ] ; Address of the BSF
                                #~ * [ GAA-Service-Identifier ] ; Service identifiers
                                #~ { Transaction-Identifier } ; B-TID
                                #~ { NAF-Id } ; NAF_ID
                                #~ [ GBA_U-Awareness-Indicator ] ; GBA_U awareness of the NAF
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ] 
                                                    
def create_pkg(config):
    return Message(16777220, "Bootstraping-Info",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
        ])

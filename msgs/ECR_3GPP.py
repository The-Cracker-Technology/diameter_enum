from libDiameter import *
from message import Message

#TS 29.272 - 7.2.19

#~ < ME-Identity-Check-Request > ::=	< Diameter Header: 324, REQ, PXY, 16777252 >
                                    #~ < Session-Id >
                                    #~ [ DRMP ]
                                    #~ [ Vendor-Specific-Application-Id ]
                                    #~ { Auth-Session-State }
                                    #~ { Origin-Host }
                                    #~ { Origin-Realm }
                                    #~ [ Destination-Host ]
                                    #~ { Destination-Realm }
                                    #~ { Terminal-Information }
                                    #~ [ User-Name ]
                                    #~ *[ AVP ]
                                    #~ *[ Proxy-Info ]
                                    #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777252, "3GPP-ME-Identity-Check",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
        ]) 

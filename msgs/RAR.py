from libDiameter import *
from message import Message

def create_pkg(config):
    return Message(0, "Re-Auth",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", dictCOMMANDname2code("Re-Auth")),
            ("Re-Auth-Request-Type", int(config["re-auth-request-type"])),
        ])

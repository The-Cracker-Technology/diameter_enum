from libDiameter import *
from message import Message

#TS 29.272 - 7.2.9

#~ < Insert-Subscriber-Data-Request> ::=		< Diameter Header: 319, REQ, PXY, 16777251 >
                                        #~ < Session-Id >
                                        #~ [ DRMP ]
                                        #~ [ Vendor-Specific-Application-Id ]
                                        #~ { Auth-Session-State }
                                        #~ { Origin-Host }
                                        #~ { Origin-Realm }
                                        #~ { Destination-Host }
                                        #~ { Destination-Realm }
                                        #~ { User-Name }
                                        #~ *[ Supported-Features]
                                        #~ { Subscription-Data}
                                        #~ [ IDR- Flags ]
                                        #~ *[ Reset-ID ]
                                        #~ *[ AVP ]
                                        #~ *[ Proxy-Info ]
                                        #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777251, "3GPP-Insert-Subscriber-Data", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["S6A"]["destination-host-mme"]),
            ("Destination-Realm", config["S6A"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config['imsi']),
            ("Subscription-Data", []),
            ("IDR-Flags", 0x18 ),
        ])

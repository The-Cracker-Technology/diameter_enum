from libDiameter import *
from message import Message

#RFC4004 - 5.3

#~ <Home-Agent-MIP-Request> ::= < Diameter Header: 262, REQ, PXY >
                                      #~ < Session-Id >
                                      #~ { Auth-Application-Id }
                                      #~ { Authorization-Lifetime }
                                      #~ { Auth-Session-State }
                                      #~ { MIP-Reg-Request }
                                      #~ { Origin-Host }
                                      #~ { Origin-Realm }
                                      #~ { User-Name }
                                      #~ { Destination-Realm }
                                      #~ { MIP-Feature-Vector }
                                      #~ [ Destination-Host ]
                                      #~ [ MIP-MN-to-HA-MSA ]
                                      #~ [ MIP-MN-to-FA-MSA ]
                                      #~ [ MIP-HA-to-MN-MSA ]
                                      #~ [ MIP-HA-to-FA-MSA ]
                                      #~ [ MIP-MSA-Lifetime ]
                                      #~ [ MIP-Originating-Foreign-AAA ]
                                      #~ [ MIP-Mobile-Node-Address ]
                                      #~ [ MIP-Home-Agent-Address ]
                                    #~ * [ MIP-Filter-Rule ]
                                      #~ [ Origin-State-Id ]
                                    #~ * [ Proxy-Info ]
                                    #~ * [ Route-Record ]
                                    #~ * [ AVP ]
                                                    
def create_pkg(config):
    return Message(2, "Home-Agent-MIP",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Auth-Application-Id", 2),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
        ])

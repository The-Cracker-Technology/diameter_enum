#RFC 6733

#~ <CER> ::= < Diameter Header: 257, REQ >
                   #~ { Origin-Host }
                   #~ { Origin-Realm }
                #~ 1* { Host-IP-Address }
                   #~ { Vendor-Id }
                   #~ { Product-Name }
                   #~ [ Origin-State-Id ]
                 #~ * [ Supported-Vendor-Id ]
                 #~ * [ Auth-Application-Id ]
                 #~ * [ Inband-Security-Id ]
                 #~ * [ Acct-Application-Id ]
                 #~ * [ Vendor-Specific-Application-Id ]
                   #~ [ Firmware-Revision ]
                 #~ * [ AVP ]

from libDiameter import *
from message import Message

def create_pkg(config):
    return Message(0, "Capabilities-Exchange", 
        [DIAMETER_HDR_REQUEST], [
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Host-IP-Address", config["host-ip-address"]),
            ("Vendor-Id", config["vendor-id"]),
            ("Product-Name", config["product-name"]),
            ("Inband-Security-Id", config["inband-security-id"]),
        ])

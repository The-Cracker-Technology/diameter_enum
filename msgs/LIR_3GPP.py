from libDiameter import *
from message import Message

#TS 29.229 - 6.1.5

#~ <Location-Info-Request> ::=		< Diameter Header: 302, REQ, PXY, 16777216 >
                                #~ < Session-Id >
                                #~ [ DRMP ]
                                #~ { Vendor-Specific-Application-Id }
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ [ Destination-Host ]
								#~ { Destination-Realm }
                                #~ [ Originating-Request ] 
                                #~ [ OC-Supported-Features ]
                                #~ *[ Supported-Features ]
                                #~ { Public-Identity }
                                #~ [ User-Authorization-Type ] 
                                #~ [ Session-Priority ]
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777216, "Location-Info",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777216)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["S6A"]["destination-host-hss"]),
            ("Destination-Realm", config["ims-url"]),
            ("Auth-Session-State", 1),
            ("Public-Identity", config['public-identity']),
        ])

from libDiameter import *
from message import Message

#TS 29.272 - 7.2.3

#~ < Update-Location-Request> ::=	< Diameter Header: 316, REQ, PXY, 16777251 >
                            #~ < Session-Id >
                            #~ [ DRMP ]
                            #~ [ Vendor-Specific-Application-Id ]
                            #~ { Auth-Session-State }
                            #~ { Origin-Host }
                            #~ { Origin-Realm }
                            #~ [ Destination-Host ]
                            #~ { Destination-Realm }
                            #~ { User-Name }
                            #~ [ OC-Supported-Features ]
                            #~ *[ Supported-Features ]
                            #~ [ Terminal-Information ]
                            #~ { RAT-Type }
                            #~ { ULR-Flags }
                            #~ [UE-SRVCC-Capability ]
                            #~ { Visited-PLMN-Id }
                            #~ [ SGSN-Number ] 
                            #~ [ Homogeneous-Support-of-IMS-Voice-Over-PS-Sessions ] 
                            #~ [ GMLC-Address ]
                            #~ *[ Active-APN ] 
                            #~ [ Equivalent-PLMN-List ]
                            #~ [ MME-Number-for-MT-SMS ]
                            #~ [ SMS-Register-Request ]
                            #~ [ SGs-MME-Identity ]
                            #~ [ Coupled-Node-Diameter-ID ]
                            #~ [ Adjacent-PLMNs ]
                            #~ [ Supported-Services ]
                            #~ *[ AVP ]
                            #~ *[ Proxy-Info ]
                            #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777251, "3GPP-Update-Location", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            #~ ("Destination-Host", config["S6A"]["destination-host"]),
            ("Destination-Realm", config["S6A"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config['imsi']),
            ("RAT-Type", "EUTRAN"),
            ("ULR-Flags", 0x22),
            ("Visited-PLMN-Id", config['plmnid'].decode("hex") ),
            ("Terminal-Information", [
                ("IMEI", config['imei']),
                ("Software-Version", "18")
            ] ),
            ("Supported-Features", [
                ("Vendor-Id", 10415),
                ("Feature-List-ID", 1),
                ("Feature-List", 0x1c000607)
            ] ),
        ])

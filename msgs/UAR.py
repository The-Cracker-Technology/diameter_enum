from libDiameter import *
from message import Message

#RFC4740 - 8.1

       #~ <UAR> ::= < Diameter Header: 283, REQ, PXY >
                 #~ < Session-Id >
                 #~ { Auth-Application-Id }
                 #~ { Auth-Session-State }
                 #~ { Origin-Host }
                 #~ { Origin-Realm }
                 #~ { Destination-Realm }
                 #~ { SIP-AOR }
                 #~ [ Destination-Host ]
                 #~ [ User-Name ]
                 #~ [ SIP-Visited-Network-Id ]
                 #~ [ SIP-User-Authorization-Type ]
               #~ * [ Proxy-Info ]
               #~ * [ Route-Record ]
               #~ * [ AVP ]

def create_pkg(config):
    return Message(6, "SIP-User-Authorization",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
            ("Auth-Session-State", 1),
        ])

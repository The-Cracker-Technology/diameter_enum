from libDiameter import *
from message import Message

#RFC4740 - 8.3

       #~ <SAR> ::= < Diameter Header: 284, REQ, PXY >
                 #~ < Session-Id >
                 #~ { Auth-Application-Id }
                 #~ { Auth-Session-State }
                 #~ { Origin-Host }
                 #~ { Origin-Realm }
                 #~ { Destination-Realm }
                 #~ { SIP-Server-Assignment-Type }
                 #~ { SIP-User-Data-Already-Available }
                 #~ [ Destination-Host ]
                 #~ [ User-Name ]
                 #~ [ SIP-Server-URI ]
               #~ * [ SIP-Supported-User-Data-Type ]
               #~ * [ SIP-AOR ]
               #~ * [ Proxy-Info ]
               #~ * [ Route-Record ]
               #~ * [ AVP ]

def create_pkg(config):
    return Message(6, "SIP-Server-Assignment",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
            ("Auth-Session-State", 1),
    #{ SIP-Server-Assignment-Type }
    #{ SIP-User-Data-Already-Available }
        ])

from libDiameter import *
from message import Message

#RFC4004 - 5.1

#~ <AA-Mobile-Node-Request> ::= < Diameter Header: 260, REQ, PXY >
                                      #~ < Session-ID >
                                      #~ { Auth-Application-Id }
                                      #~ { User-Name }
                                      #~ { Destination-Realm }
                                      #~ { Origin-Host }
                                      #~ { Origin-Realm }
                                      #~ { MIP-Reg-Request }
                                      #~ { MIP-MN-AAA-Auth }
                                      #~ [ Acct-Multi-Session-Id ]
                                      #~ [ Destination-Host ]
                                      #~ [ Origin-State-Id ]
                                      #~ [ MIP-Mobile-Node-Address ]
                                      #~ [ MIP-Home-Agent-Address ]
                                      #~ [ MIP-Feature-Vector ]
                                      #~ [ MIP-Originating-Foreign-AAA ]
                                      #~ [ Authorization-Lifetime ]
                                      #~ [ Auth-Session-State ]
                                      #~ [ MIP-FA-Challenge ]
                                      #~ [ MIP-Candidate-Home-Agent-Host ]
                                      #~ [ MIP-Home-Agent-Host ]
                                      #~ [ MIP-HA-to-FA-SPI ]
                                    #~ * [ Proxy-Info ]
                                    #~ * [ Route-Record ]
                                    #~ * [ AVP ]

def create_pkg(config):
    return Message(2, "AA-Mobile-Node",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Auth-Application-Id", 2),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
        ])

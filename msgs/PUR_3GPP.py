from libDiameter import *
from message import Message

#TS 29.329 - 6.1.3

#~ < Profile-Update-Request > ::=		< Diameter Header: 307, REQ, PXY, 16777217 >
                                #~ < Session-Id >
                                #~ [ DRMP ]
                                #~ { Vendor-Specific-Application-Id }
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ [ Destination-Host ]
                                #~ { Destination-Realm }
                                #~ *[ Supported-Features ]
                                #~ { User-Identity }
                                #~ [ Wildcarded-Public-Identity ]
                                #~ [ Wildcarded-IMPU ] 
                                #~ [ User-Name ]
                                #~ *{ Data-Reference }
                                #~ { User-Data }
                                #~ [ OC-Supported-Features ]
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777217, "Profile-Update",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777217)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
    #{ User-Identity }
    #{ Data-Reference }
    #{ User-Data}
        ])

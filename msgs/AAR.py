from libDiameter import *
from message import Message

#RFC7155 - 3.1

#~ <AA-Request> ::= < Diameter Header: 265, REQ, PXY >
                          #~ < Session-Id >
                          #~ { Auth-Application-Id }
                          #~ { Origin-Host }
                          #~ { Origin-Realm }
                          #~ { Destination-Realm }
                          #~ { Auth-Request-Type }
                          #~ [ Destination-Host ]
                          #~ [ NAS-Identifier ]
                          #~ [ NAS-IP-Address ]
                          #~ [ NAS-IPv6-Address ]
                          #~ [ NAS-Port ]
                          #~ [ NAS-Port-Id ]
                          #~ [ NAS-Port-Type ]
                          #~ [ Origin-AAA-Protocol ]
                          #~ [ Origin-State-Id ]
                          #~ [ Port-Limit ]
                          #~ [ User-Name ]
                          #~ [ User-Password ]
                          #~ [ Service-Type ]
                          #~ [ State ]
                          #~ [ Authorization-Lifetime ]
                          #~ [ Auth-Grace-Period ]
                          #~ [ Auth-Session-State ]
                          #~ [ Callback-Number ]
                          #~ [ Called-Station-Id ]
                          #~ [ Calling-Station-Id ]
                          #~ [ Originating-Line-Info ]
                          #~ [ Connect-Info ]
                          #~ [ CHAP-Auth ]
                          #~ [ CHAP-Challenge ]
                        #~ * [ Framed-Compression ]
                          #~ [ Framed-Interface-Id ]
                          #~ [ Framed-IP-Address ]
                        #~ * [ Framed-IPv6-Prefix ]
                          #~ [ Framed-IP-Netmask ]
                          #~ [ Framed-MTU ]
                          #~ [ Framed-Protocol ]
                          #~ [ ARAP-Password ]
                          #~ [ ARAP-Security ]
                        #~ * [ ARAP-Security-Data ]
                        #~ * [ Login-IP-Host ]
                        #~ * [ Login-IPv6-Host ]
                          #~ [ Login-LAT-Group ]
                          #~ [ Login-LAT-Node ]
                          #~ [ Login-LAT-Port ]
                          #~ [ Login-LAT-Service ]
                        #~ * [ Tunneling ]
                        #~ * [ Proxy-Info ]
                        #~ * [ Route-Record ]
                        #~ * [ AVP ]


def create_pkg(config):
    return Message(3, "AA",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 1),
            ("Auth-Request-Type", int(config["auth-request-type"])),
        ])

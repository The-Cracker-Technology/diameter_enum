from libDiameter import *
from message import Message

#TS 29.229 - 6.1.3

#~ <Server-Assignment-Request> ::=	< Diameter Header: 301, REQ, PXY, 16777216 >
                                #~ < Session-Id >
                                #~ [ DRMP ]
                                #~ { Vendor-Specific-Application-Id }
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ [ Destination-Host ]
                                #~ { Destination-Realm }
                                #~ [ User-Name ] 
                                #~ [ OC-Supported-Features ]
                                #~ *[ Supported-Features ]
                                #~ *[ Public-Identity ]
                                #~ [ Wildcarded-Public-Identity ]
                                #~ { Server-Name }
                                #~ { Server-Assignment-Type }
                                #~ { User-Data-Already-Available }
                                #~ [ SCSCF-Restoration-Info ]
                                #~ [ Multiple-Registration-Indication ] 
                                #~ [ Session-Priority ] 
                                #~ [ SAR-Flags ]
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777216, "Server-Assignment",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777216)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
    
    #{ Server-Name }
    #{ Server-Assignment-Type }
    #{ User-Data-Already-Available }
        ])

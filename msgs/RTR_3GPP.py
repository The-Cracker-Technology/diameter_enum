from libDiameter import *
from message import Message

#TS 29.229 - 6.1.9

#~ <Registration-Termination-Request> ::=		< Diameter Header: 304, REQ, PXY, 16777216 >
									#~ < Session-Id >
                                    #~ [ DRMP ]
									#~ { Vendor-Specific-Application-Id }
                                    #~ { Auth-Session-State }
                                    #~ { Origin-Host }
                                    #~ { Origin-Realm }
                                    #~ { Destination-Host }
                                    #~ { Destination-Realm }
                                    #~ { User-Name }
                                    #~ [ Associated-Identities ]
                                    #~ *[ Supported-Features ]
                                    #~ *[ Public-Identity ]
                                    #~ { Deregistration-Reason }
                                    #~ *[ AVP ]
                                    #~ *[ Proxy-Info ]
                                    #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777216, "Registration-Termination",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777216)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
            ("Auth-Session-State", 1),
    #{ User-Name }
    #{ Deregistration-Reason }
        ])

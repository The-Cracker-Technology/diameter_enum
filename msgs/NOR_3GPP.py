from libDiameter import *
from message import Message

#TS 29.272 - 7.2.17

#~ < Notify-Request> ::=	< Diameter Header: 323, REQ, PXY, 16777251 >
                        #~ < Session-Id >
                        #~ [ Vendor-Specific-Application-Id ]
                        #~ [ DRMP ]
                        #~ { Auth-Session-State }
                        #~ { Origin-Host }
                        #~ { Origin-Realm }
                        #~ [ Destination-Host ]
                        #~ { Destination-Realm }
                        #~ { User-Name }
                        #~ [ OC-Supported-Features ]
                        #~ * [ Supported-Features ]
                        #~ [ Terminal-Information ]
                        #~ [ MIP6-Agent-Info ] 
                        #~ [ Visited-Network-Identifier ]
                        #~ [ Context-Identifier ]
                        #~ [Service-Selection]
                        #~ [ Alert-Reason ]
                        #~ [ UE-SRVCC-Capability ]
                        #~ [ NOR-Flags ]
                        #~ [ Homogeneous-Support-of-IMS-Voice-Over-PS-Sessions ]
                        #~ [ Maximum-UE-Availability-Time ]
                        #~ *[ Monitoring-Event-Config-Status ]
                        #~ [ Emergency-Services ]
                        #~ *[ AVP ]
                        #~ *[ Proxy-Info ]
                        #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777251, "3GPP-Notify", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777251)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host-mme"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["S6A"]["destination-host-bedsa"]),
            ("Destination-Realm", config["S6A"]["destination-realm"]),
            ("Auth-Session-State", 1),
            ("User-Name", config['imsi']),
            ("NOR-Flags", 0x111),
        ])

from libDiameter import *
from message import Message

#TS 29.338 - 5.3.2.3

#~ < Send-Routing-Info-for-SM-Request > ::= < Diameter Header: 8388647, REQ, PXY, 16777312 >
                                        #~ < Session-Id >
                                        #~ [ DRMP ]
                                        #~ [ Vendor-Specific-Application-Id ]
                                        #~ { Auth-Session-State }
                                        #~ { Origin-Host }
                                        #~ { Origin-Realm }
                                        #~ [ Destination-Host ]
                                        #~ { Destination-Realm }
                                        #~ [ MSISDN ]
                                        #~ [ User-Name ]
                                        #~ [ SMSMI-Correlation-ID ]
                                        #~ *[ Supported-Features ]
                                        #~ [ SC-Address ]
                                        #~ [ SM-RP-MTI ]
                                        #~ [ SM-RP-SMEA ]
                                        #~ [ SRR-Flags ]
                                        #~ [ SM-Delivery-Not-Intended ]
                                        #~ *[ AVP ]
                                        #~ *[ Proxy-Info ]
                                        #~ *[ Route-Record ]

def create_pkg(config):
    return Message(16777312, "3GPP-Routing-Info-For-SM", 
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777312)
            ] ),
            ("Origin-Host", config["S6A"]["origin-host"]),
            ("Origin-Realm", config["S6A"]["origin-realm"]),
            ("Destination-Host", config["S6A"]["destination-host-hss"]),
            ("Destination-Realm", config["S6A"]["destination-realm-ims"]),
            ("Auth-Session-State", 1),
            ("User-Name", config["imsi"]),
        ])

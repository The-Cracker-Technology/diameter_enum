from libDiameter import *
from message import Message

#TS 29.229 - 6.1.1

#~ < User-Authorization-Request> ::=			< Diameter Header: 300, REQ, PXY, 16777216 >
								#~ < Session-Id >
                                #~ [ DRMP ]
								#~ { Vendor-Specific-Application-Id }
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ [ Destination-Host ]
                                #~ { Destination-Realm }
                                #~ { User-Name }
                                #~ [ OC-Supported-Features ]
                                #~ *[ Supported-Features ]
                                #~ { Public-Identity }
                                #~ { Visited-Network-Identifier }
                                #~ [ User-Authorization-Type ]
                                #~ [ UAR-Flags ]
								#~ *[ AVP ]
								#~ *[ Proxy-Info ]
                                #~ *[ Route-Record ]

def create_pkg(config):    
    return Message(16777216, "User-Authorization",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777216)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            #~ ("Destination-Host", config["S6A"]["destination-host-hss"]),
            ("Destination-Realm", config["S6A"]["destination-realm-ims"]),
            ("Auth-Session-State", 1),
            ("User-Name", config["username"]),
            ("Public-Identity", config["public-identity"]),
            ("Visited-Network-Identifier", config["ims-url"]),
        ])

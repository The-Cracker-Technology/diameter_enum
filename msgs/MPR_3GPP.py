from libDiameter import *
from message import Message

#TS 29.140 - 6.1.1

#~ <Message-Process-Request> ::= < Diameter Header: 311, REQ, PXY, 16777226 >
                                #~ < Session-Id >
                                #~ { Vendor-Specific-Application-Id }
                                #~ { Auth-Session-State }
                                #~ { Origin-Host }
                                #~ { Origin-Realm }
                                #~ { Destination-Host }
                                #~ { Destination-Realm }
                                #~ { Event-Timestamp }
                                #~ { Trigger-Event }
                                #~ { Served-User-Identity }
                                #~ [3GPP-IMSI ]
                                #~ [ Sender-Address ]
                                #~ *{ Initial-Recipient-Address }
                                #~ { Originating-Interface }
                                #~ [Service-Key] 
                                #~ [ Delivery-Report ]
                                #~ [ Read-Reply ]
                                #~ [ Sender-Visibility ]
                                #~ *[ AVP ]
                                #~ *[ Proxy-Info ]
                                #~ *[ Route-Record ] 
                                                    
def create_pkg(config):
    return Message(16777226, "Message-Process",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777226)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
    #{ Event-Timestamp }
    #{ Trigger-Event }
    #{ Served-User-Identity }
    #{ Initial-Recipient-Address }
    #{ Originating-Interface }
        ])

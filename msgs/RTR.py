from libDiameter import *
from message import Message

#RFC4740 - 8.9

       #~ <RTR> ::= < Diameter Header: 287, REQ, PXY >
                 #~ < Session-Id >
                 #~ { Auth-Application-Id }
                 #~ { Auth-Session-State }
                 #~ { Origin-Host }
                 #~ { Origin-Realm }
                 #~ { Destination-Host }
                 #~ { SIP-Deregistration-Reason }
                 #~ [ Destination-Realm ]
                 #~ [ User-Name ]
               #~ * [ SIP-AOR ]
               #~ * [ Proxy-Info ]
               #~ * [ Route-Record ]
               #~ * [ AVP ]

def create_pkg(config):
    return Message(6, "SIP-Registration-Termination",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Application-Id", 6),
            ("Auth-Session-State", 1),
    #{ SIP-Deregistration-Reason }
        ])

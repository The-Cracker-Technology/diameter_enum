from libDiameter import *
from message import Message

#TS 29.172 - 7.3.3

#~ < Location-Report-Request> ::=	< Diameter Header: 8388621, REQ, PXY, 16777255 >
                                    #~ < Session-Id >
                                    #~ [ Vendor-Specific-Application-Id ]
                                    #~ { Auth-Session-State }
                                    #~ { Origin-Host }
                                    #~ { Origin-Realm }
                                    #~ { Destination-Host }
                                    #~ { Destination-Realm }
                                    #~ { Location-Event }
                                    #~ [ LCS-EPS-Client-Name ]
                                    #~ [ User-Name ]
                                    #~ [ MSISDN] 
                                    #~ [ IMEI ]
                                    #~ [ Location-Estimate ]
                                    #~ [ Accuracy-Fulfilment-Indicator ]
                                    #~ [ Age-Of-Location-Estimate ]
                                    #~ [ Velocity-Estimate ]
                                    #~ [ EUTRAN-Positioning-Data ]
                                    #~ [ ECGI]
                                    #~ [ LCS-Service-Type-ID ]
                                    #~ [ Pseudonym-Indicator ]
                                    #~ [ LCS-QoS-Class ]
                                    #~ [ Serving-Node ]
                                    #~ *[ Supported-Features ]
                                    #~ *[ AVP ]
                                    #~ *[ Proxy-Info ]
                                    #~ *[ Route-Record ]
                                                    
def create_pkg(config):
    return Message(16777255, "3GPP-Location-Report",
        [DIAMETER_HDR_PROXIABLE, DIAMETER_HDR_REQUEST], [
            ("Session-Id", config["session-id"]),
            ("Vendor-Specific-Application-Id", [
                ("Vendor-Id", 10415),
                ("Auth-Application-Id", 16777255)
            ] ),
            ("Origin-Host", config["origin-host"]),
            ("Origin-Realm", config["origin-realm"]),
            ("Destination-Host", config["destination-host"]),
            ("Destination-Realm", config["destination-realm"]),
            ("Auth-Session-State", 1),
        ])
